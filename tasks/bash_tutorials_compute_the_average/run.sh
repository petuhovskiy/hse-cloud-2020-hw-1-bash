#!/bin/bash

read -r N

SUM=0

for LINE in `cat`
do
    SUM=$(($SUM + $LINE))
done

printf '%.3f\n' "$((10**3 * $SUM / $N))e-3"
