#!/bin/bash

read -r EXPR

printf %.3f "$(echo $EXPR | bc -q -l)"
