#!/bin/bash

read -r choice
case "$choice" in 
  y|Y ) echo "YES";;
  n|N ) echo "NO";;
  * ) echo "???";;
esac
